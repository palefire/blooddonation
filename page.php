<?php
	get_header();
	if( have_posts() ) :
		while( have_posts() ):
			the_post();
			?>
			<section class="blood-campaign">
				<div class="blood-campaign-content">
					<h1>Blood Campaign</h1>
					<p>Let us craft your dreams into reality. We listen to you and welcome your ideas. Come visit us and you will see that all are true.</p>
				</div>
				<div class="left-img">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/image/blood1.png">
				</div>
				<div class="middle-img">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/image/cross.png">
				</div>
				<div class="right-img">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/image/blood.png">
				</div>
			</section>


			<section class="home-container">
				<div class="row" style="margin-left:0; margin-right:0">
					<div class="col-md-3 col-0 left-container" style="background-image:url(<?php echo get_template_directory_uri(); ?>/assets/image/banner-blood.jpg)"></div>
					<div class="col-md-9 col-12 right-container">
						<div class="full-form">
							<h2>It's a pleasure to meet you.</h2>
							<?php echo do_shortcode( '[formidable id=2]' );  ?>
						</div>
					</div>
				</div>
			</section>

			<?php
		endwhile;
	endif;
	get_footer();