<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="pingback" href="<?php esc_url(bloginfo( 'pingback_url' )); ?>">
	<?php wp_head(); ?>

</head>
<body <?php body_class(); ?>>
    <header>
        <!-- <nav class="navbar navbar-expand-lg navbar-light ">
            <a class="navbar-brand" href="#">Bagnan Yubakbrindo</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav">
                    <li class="nav-item active">
                        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Form</a>
                    </li>
                   
                </ul>
            </div>
        </nav> -->
        <div class="row laptop" style="">
            <div class="col-md-4 logo">
                 <a href=""> <img src="<?php echo get_template_directory_uri(); ?>/assets/image/logo.png"></a>
            </div>
            <div class="col-md-4 club">
                <h2>বাগনান যুবক-বৃন্দ</h2>
            </div>
            <div class="col-md-4 social">
                <a href=""> <img src="<?php echo get_template_directory_uri(); ?>/assets/image/facebook.png"></a>
            </div>
        </div>
        <div class="row mobile" style="margin-left:0; margin-right:0">
            <div class="col-4 logo">
                <a href=""> <img src="<?php echo get_template_directory_uri(); ?>/assets/image/logo.png"></a>
            </div>
            <div class="col-4 club">
                <h2>বাগনান যুবক-বৃন্দ</h2>
            </div>
            <div class=" col-4 social">
                 <a href=""> <img src="<?php echo get_template_directory_uri(); ?>/assets/image/facebook.png"></a>
            </div>
        </div>
    </header>