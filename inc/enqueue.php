<?php
	function bd_load_scripts(){
		//css		
		wp_enqueue_style('bootstrap','https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css',array(),'4.4.1','all');
		wp_enqueue_style('style',get_stylesheet_uri(),array(),microtime(),'all');
		wp_enqueue_style('responsive',get_template_directory_uri().'/assets/css/responsive.css',array(),microtime(),'all');
		// js
		wp_enqueue_script('bootstrap','https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js',array('jquery'),'1.16.0',true);
		wp_enqueue_script('popper','https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js',array('jquery'),'4.4.1',true);
   	}
	add_action('wp_enqueue_scripts','bd_load_scripts');